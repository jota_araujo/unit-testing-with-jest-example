/*jslint node:true, mocha:true */

const sinon = require('sinon')
const chai = require('chai')
chai.use(require('chai-as-promised'))

const Forbidden = require('http-errors').Forbidden
const BadRequest = require('http-errors').BadRequest

const expect = chai.expect
const assert = chai.assert
//require('sinon-mongoose')

var Mongoose = require('mongoose').Mongoose
var mockgoose = require('mockgoose')
var mongoose = new Mongoose()

const Coupon = require('../models/coupon')
const Product = require('../models/product')
const Purchase = require('../models/purchase')
const isCouponValid = require('./coupon').couponIsValid
const CouponErrors = require('./coupon').CouponErrors
const ProductMock = mongoose.model('Product', Product.schema)
const CouponMock = mongoose.model('Coupon', Coupon.schema)

const inactiveProduct = {
  name: 'inactive_product',
  category: 'coupon',
  price: 0,
  active: false
}

const activeProduct = {
  name: 'active_product',
  category: 'coupon',
  price: 0,
  active: true
}

const dummyCoupon = {
  code: '123456'
}

before(function(done) {
  mockgoose(mongoose).then(function () {
    console.log(mongoose.isMocked)
    mongoose.connect('mongodb://1234', function (err) {
      console.log('connected to mongodb mock')
      ProductMock.create([activeProduct, inactiveProduct],
      function (err, product) {
        if (!err) {
          CouponMock.create(dummyCoupon,
          function (err, product) {
            if (!err) {
              // console.log("setup is done")
              done()
            }
          })
        }
      })
    })
	})
})

describe('validatedCouponHook', function () {
    it('throws BadRequest error if data param does not have the coupon data', function () {
      const p = isCouponValid(ProductMock, CouponMock, null)
      return expect(p).to.eventually.be.rejectedWith(CouponErrors.invalidCouponData)
    })

    it('reject if data param does not have the coupon name', function () {
      const p = isCouponValid(ProductMock, CouponMock, {'foo': 1, 'bar': 2})
      return expect(p).to.eventually.be.rejectedWith(CouponErrors.couponNameIsMissing)
    })

    it('reject if data param does not have the coupon code', function () {
      const p = isCouponValid(ProductMock, CouponMock, {'name': 1})
      return expect(p).to.eventually.be.rejectedWith(CouponErrors.couponCodeIsMissing)
    })

    it('throws NotAcceptable error if an invalid coupon code is provided', function () {
      const options = {
        category: 'coupon',
        name: 'active_product',
        code: '12345'
      }
      const p = isCouponValid(ProductMock, CouponMock, options)
      return expect(p).to.eventually.be.rejectedWith(CouponErrors.invalidCouponCode)
    })

    // TODO: it('responds with the proper error if a product id provided was not found in the db')

    it('responds with the proper error if a product id provided is not active', function () {
      const options = {
        category: 'coupon',
        name: 'inactive_product',
        code: '123456'
      }
      const p = isCouponValid(ProductMock, CouponMock, options)
      return expect(p).to.eventually.be.rejectedWith(CouponErrors.productNotActive)
    })

    it('returns true if valid coupon data is provided', function () {
      const options = {
        category: 'coupon',
        name: 'active_product',
        code: '123456'
      }
      const p = isCouponValid(ProductMock, CouponMock, options)
      return expect(p).to.eventually.be.equal(true)
    })

  })

// TODO: it should  not validate if option.coupon not a String

// TODO: it should  note validate if no coupon is found

// TODO: it should  validate is the value of the coupon matches an existing coupon from the db
