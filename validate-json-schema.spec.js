

import factory from './validate-json-schema'

const hook = {
  method: 'create',
}
describe('Given a validate hook: ', () => {
  beforeEach(() => {
    hook.service = {
      config: {},
    }
  })

  test('it resolves if the input contains a valid schema',
        async () => {
          expect.assertions(1)
          hook.service.config.schemas = {
            create: {
              properties: {
                foo: {
                  type: 'string',
                },
                bar: {
                  type: 'integer',
                },
              },
              required: ['foo', 'bar'],
            },
          }
          return expect(factory()(hook)).resolves.toBeDefined()
        })

  test('it rejects if the input contains an invalid schema',
        async () => {
          expect.assertions(1)
          hook.service.config.schemas = {
            create: {
              properties: {
                foo: {
                  type: 'string',
                },
                bar: {
                  type: 'some-invalid-type',
                },
              },
              required: ['foo', 'bar'],
            },
          }
          return expect(factory()(hook)).rejects.toBeDefined()
        })
})

describe('Given a validate hook: ', () => {
  beforeAll(() => {
    hook.service = {
      config: {},
    }
  })
  test('it skips validation if no schema is provided',
      async () => {
        expect.assertions(1)
        const theModule = require('./../validate-json-schema')
        theModule.validate = jest.fn()
        await theModule.default()(hook)
        expect(theModule.validate).toHaveBeenCalledTimes(0)
      })
})
