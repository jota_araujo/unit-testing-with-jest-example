import factory from './check-if-in-use'

jest.mock('./commons/views')

const hook = {}


describe('Given a view returns a meta.inUse attribute: ', () => {
  beforeEach(() => {
    hook.params = {
      id: 'anything',
    }
  })

  test('it rejects meta.inUse is true',
      async () => {
        expect.assertions(1)
        hook.data = {
          meta: {
            inUse: true,
          },
        }
        return expect(factory()(hook)).rejects.toBeDefined()
      })

  test('it does not reject if meta.inUse is false',
       () => {
         expect.assertions(1)
         hook.data = {
           meta: {
             inUse: false,
           },
         }
         return expect(factory()(hook)).resolves.toBeDefined()
       })
})
