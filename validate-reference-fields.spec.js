import factory from './validate-reference-fields'

const hook = {}

describe('Given a validate hook: ', () => {
  beforeEach(() => {
    hook.params = {}
    hook.service = {}
    hook.data = {}
  })

  test('it creates a referenceFields attribute from data._referenceFields',
    async () => {
      expect.assertions(2)
      hook.service.find = () => Promise.resolve({
        rows: [1, 2],
      })
      hook.data._referenceFields = ['a', 'b']
      expect(factory()(hook)).resolves.toBeDefined()
      return expect(hook.data.referenceFields).toHaveLength(2)
    })

  test('it creates a referenceFields attribute from hook.params.scope',
    async () => {
      expect.assertions(2)
      hook.service.find = () => Promise.resolve({
        rows: new Array(2),
      })
      hook.params.scope = {
        key1: 'val1',
        key2: 'val2',
      }
      const prom = factory()(hook)
      expect(prom).resolves.toBeDefined()
      return expect(hook.data.referenceFields).toHaveLength(2)
    })

  test('it ignores "na" values from hook.params.scope',
    async () => {
      expect.assertions(2)
      hook.service.find = () => Promise.resolve({
        rows: new Array(2),
      })
      hook.params.scope = {
        key1: 'val1',
        key2: 'val2',
        key3: 'na',
      }
      const prom = factory()(hook)
      expect(prom).resolves.toBeDefined()
      return expect(hook.data.referenceFields).toHaveLength(2)
    })

  test('it creates a referenceFields attribute combining from hook.params.scope and data._referenceFields',
    async () => {
      expect.assertions(2)
      hook.service.find = () => Promise.resolve({
        rows: new Array(3),
      })
      hook.params.scope = {
        key1: 'val1',
        key2: 'val2',
      }
      hook.data._referenceFields = ['a']
      const prom = factory()(hook)
      expect(prom).resolves.toBeDefined()
      return expect(hook.data.referenceFields).toHaveLength(3)
    })

  test('it rejects if a view returns less results than the amount expected',
    async () => {
      expect.assertions(1)
      hook.service.find = () => Promise.resolve({
        rows: new Array(1),
      })
      hook.params.scope = {
        key1: 'val1',
        key2: 'val2',
      }
      hook.data._referenceFields = ['a']
      const prom = factory()(hook)
      return expect(prom).rejects.toBeDefined()
    })
  test('it rejects if a view does not return a rows property',
    async () => {
      expect.assertions(1)
      hook.service.find = () => Promise.resolve({})
      hook.data._referenceFields = ['a']
      const prom = factory()(hook)
      return expect(prom).rejects.toBeDefined()
    })
})
